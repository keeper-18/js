var express = require('express'),
    app = express(),
    port = 4000;

app.use('/public', express.static(__dirname + '/public'));
app.use('/views', express.static(__dirname + '/views'));
app.set('view engine', 'pug');

app.get('/', function(req, res){
    res.render('index');
})
app.listen(port, function(){
    console.log('server listen on ' + port);
});